# cbot #

This is a Telegram bot written in C.
Licensed under AGPLv3+.

## Building ##

Requirements:

* C compiler compliant with the C99 standard.
* [libcurl](https://curl.haxx.se)
* [json-c](https://github.com/json-c/json-c)

Recommended:

* [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)

Building:

1. Make the required changes for your environment in `config.mk`.
2. Make the required changes in `h/config.h.in`.
3. Run `make` or `make -j$(nproc)` to compile the bot.
4. Run `./cbot` to run the bot.
