/* unicode_util.c -- unicode_util.h implementation.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include "unicode_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <unicode/ustring.h>

#include "cbot.h"

UChar *
u_strndup(const UChar *s, size_t n)
{
	UChar *ret;

	ret = calloc(n + 1, sizeof(UChar));
	if (ret == NULL) {
		eputs("Allocation error");
		return NULL;
	}

	u_strncpy(ret, s, n);
	ret[n] = '\0';
	return ret;
}
