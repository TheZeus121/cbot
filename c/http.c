/* http.c -- Helper functions for HTTP calls.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <curl/curl.h>
#include <json_tokener.h>
#include <json_util.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cbot.h"

/* TODO: I could probably build the JSON object while downloading the
 * data, instead of building it afterwards from the resulting string,
 * have to look into this */

typedef struct {
	char  *mem;
	size_t size;
	size_t cap;
} Vector;

static size_t
write_callback(void *data, size_t size, size_t nmemb, void *userp)
{
	size_t  fullsize, newsize, newcap;
	Vector *vec;
	char   *ptr;

	fullsize = size * nmemb;
	vec = (Vector *) userp;
	newsize = vec->size + fullsize + 1;
	if (newsize > vec->cap) {
		newcap = vec->cap;
		do {
			newcap <<= 1;
		} while (newcap < newsize);

		ptr = realloc(vec->mem, newcap);
		if (ptr == NULL) {
			eprintf("realloc failed: %s\n", strerror(errno));
			return 0;
		}

		vec->mem = ptr;
		vec->cap = newcap;
	}

	memcpy(&(vec->mem[vec->size]), data, fullsize);
	vec->size += fullsize;
	vec->mem[vec->size] = 0;

	return fullsize;
}

extern json_object *
http_getjson(const char *url)
{
	char *str;
	json_object *root, *jobj, *ret;
	enum json_tokener_error jerr;
	enum json_type jtype;

	str = http_getstr(url);
	if (str == NULL) {
		return NULL;
	}

	ret = NULL;
	root = json_tokener_parse_verbose(str, &jerr);
	if (jerr != json_tokener_success) {
		eprintf("JSON parse: %s\n", json_tokener_error_desc(jerr));
		eprintf("    '%s'\n", str);
		goto end_str;
	}

	if (!json_object_object_get_ex(root, "ok", &jobj)) {
		eprintf("'ok' key not found in JSON '%s'\n", str);
		goto end_json;
	}

	jtype = json_object_get_type(jobj);
	if (jtype != json_type_boolean) {
		eprintf("'ok' type expected to be a boolean, got %s instead!\n",
		        json_type_to_name(jtype));
		eprintf("    '%s'\n", str);
		goto end_json;
	}

	if (json_object_get_boolean(jobj)) {
		if (!json_object_object_get_ex(root, "result", &ret)) {
			eprintf("'result' key not found in JSON '%s'\n", str);
			ret = NULL;
			goto end_json;
		}

		json_object_get(ret);
	} else {
		eprintf("API Error: %s\n", str);
	}

end_json:
	json_object_put(root);

end_str:
	free(str);
	return ret;
}

extern char*
http_getstr(const char *url)
{
	CURL *curl;
	CURLcode res;
	char *ret;
	Vector vec;

	curl = curl_easy_init();
	if (curl == NULL) {
		eputs("curl_easy_init failed.");
		return NULL;
	}

	vec.mem  = malloc(256);
	vec.cap  = 256;
	vec.size = 0;

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &vec);

	res = curl_easy_perform(curl);
	if (res != CURLE_OK) {
		eprintf("curl_easy_perform failed: %s\n",
		        curl_easy_strerror(res));
		ret = NULL;
	} else {
		ret = vec.mem;
	}

	curl_easy_cleanup(curl);
	return ret;
}
