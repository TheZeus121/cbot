/* encode.c -- various helper functions for encoding text.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cbot.h"

static unsigned char
to_hex(unsigned char a)
{
	if (a < 10) {
		return a + '0';
	} else {
		return a - 10 + 'A';
	}
}

char *
encode_url(const char *str)
{
	size_t len, srci, reti;
	char *ret;
	unsigned char c;

	len = strlen(str);
	ret = calloc(len * 3 + 1, sizeof(char));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	for (srci = 0, reti = 0; srci < len; srci++) {
		c = str[srci];
		if (isalnum(c) || c == '*' || c == '-'
		    || c == '.' || c == '_') {
			ret[reti++] = c;
		} else if (c == ' ') {
			ret[reti++] = '+';
		} else {
			ret[reti++] = '%';
			ret[reti++] = to_hex(c / 16);
			ret[reti++] = to_hex(c % 16);
		}
	}

	ret[reti] = '\0';
	return ret;
}
