/* parse.c -- Implementation of functions from parse.h
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include "parse.h"

#include <stdio.h>
#include <stdlib.h>

#include "cbot.h"

UChar *
string_parse(json_object *json)
{
	const char *str;
	int32_t cap;
	UChar *ret;
	UErrorCode err;

	err = U_ZERO_ERROR;
	str = json_object_get_string(json);
	u_strFromUTF8Lenient(NULL, 0, &cap, str, -1, &err);
	if (U_FAILURE(err) && err != U_BUFFER_OVERFLOW_ERROR) {
		eprintf("Unicode error: %s\n", u_errorName(err));
		return NULL;
	}

	ret = calloc(cap + 1, sizeof(UChar));
	if (ret == NULL) {
		eputs("Allocation error");
		return NULL;
	}

	err = U_ZERO_ERROR;
	u_strFromUTF8Lenient(ret, cap, NULL, str, -1, &err);
	if (U_FAILURE(err)) {
		free(ret);
		eprintf("Unicode error: %s\n", u_errorName(err));
		return NULL;
	}

	return ret;
}
