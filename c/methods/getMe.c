/* getMe.c -- implementation of /getMe from Telegram API.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <stdlib.h>

#include "api.h"
#include "cbot.h"
#include "config.h"

User *
api_getMe(void)
{
	json_object *jobj;
	User *usr;

	jobj = http_getjson(API_URL"/getMe");
	if (jobj == NULL) {
		return NULL;
	}

	usr = User_parse(jobj);
	json_object_put(jobj);
	return usr;
}
