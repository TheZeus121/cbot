/* getUpdates.c -- implementation of /getUpdates from Telegram API.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "config.h"

#define STR(x) _STR(x)
#define _STR(x) #x

#define URL API_URL"/getUpdates?offset=%d&timeout="STR(TIMEOUT)

Update **
api_getUpdates(int offset)
{
	int len;
	char *url;
	json_object *json;
	Update **ret;

	len = snprintf(NULL, 0, URL, offset);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		return NULL;
	}

	len++;
	url = calloc(len, sizeof(char));
	if (url == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	len = snprintf(url, len, URL, offset);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(url);
		return NULL;
	}

	json = http_getjson(url);
	free(url);
	if (json == NULL) {
		return NULL;
	}

	ret = UpdateArray_parse(json);
	json_object_put(json);
	return ret;
}

#undef URL
#define URL API_URL"/getUpdates?offset=%d"

Update **
api_getUpdatesShortPoll(int offset)
{
	int len;
	char *url;
	json_object *json;
	Update **ret;

	len = snprintf(NULL, 0, URL, offset);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		return NULL;
	}

	len++;
	url = calloc(len, sizeof(char));
	if (url == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	len = snprintf(url, len, URL, offset);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(url);
		return NULL;
	}

	json = http_getjson(url);
	free(url);
	if (json == NULL) {
		return NULL;
	}

	ret = UpdateArray_parse(json);
	json_object_put(json);
	return ret;
}
