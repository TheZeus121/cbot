/* sendMessage.c -- Implementations of the /sendMessage Telegram API.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "config.h"

#define URL API_URL"/sendMessage?chat_id=%"PRId64"&text=%s"

Message *
api_sendMessage(int64_t chat_id, const char *text)
{
	int len;
	char *url, *enc_text;
	json_object *json;
	Message *ret;

	enc_text = encode_url(text);
	if (enc_text == NULL) {
		return NULL;
	}
	
	len = snprintf(NULL, 0, URL, chat_id, enc_text);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len++;
	url = calloc(len, sizeof(char));
	if (url == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len = snprintf(url, len, URL, chat_id, enc_text);
	free(enc_text);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(url);
		return NULL;
	}

	json = http_getjson(url);
	free(url);
	if (json == NULL) {
		return NULL;
	}

	ret = Message_parse(json);
	json_object_put(json);
	return ret;
}

#undef URL
#define URL API_URL"/sendMessage?chat_id=%"PRId64"&text=%s&parse_mode=%s"

Message *
api_sendFormattedMessage(int64_t chat_id, const char *text,
                         ParseMode parse_mode)
{
	int len;
	char *url, *enc_text;
	const char *parse_mode_str;
	json_object *json;
	Message *ret;

	parse_mode_str = (parse_mode == HTML) ? "HTML" : "Markdown";
	enc_text = encode_url(text);
	if (enc_text == NULL) {
		return NULL;
	}

	len = snprintf(NULL, 0, URL, chat_id, enc_text, parse_mode_str);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len++;
	url = calloc(len, sizeof(char));
	if (url == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len = snprintf(url, len, URL, chat_id, enc_text, parse_mode_str);
	free(enc_text);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		return NULL;
	}

	json = http_getjson(url);
	free(url);
	if (json == NULL) {
		return NULL;
	}

	ret = Message_parse(json);
	json_object_put(json);
	return ret;
}

#undef URL
#define URL API_URL"/sendMessage?chat_id=%"PRId64"&text=%s" \
            "&reply_to_message_id=%d"

Message *
api_sendReply(Message *msg, const char *text)
{
	int len;
	char *url, *enc_text;
	json_object *json;
	Message *ret;

	enc_text = encode_url(text);
	if (enc_text == NULL) {
		return NULL;
	}
	
	len = snprintf(NULL, 0, URL, msg->chat->id, enc_text, msg->message_id);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len++;
	url = calloc(len, sizeof(char));
	if (url == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len = snprintf(url, len, URL, msg->chat->id, enc_text, msg->message_id);
	free(enc_text);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(url);
		return NULL;
	}

	json = http_getjson(url);
	free(url);
	if (json == NULL) {
		return NULL;
	}

	ret = Message_parse(json);
	json_object_put(json);
	return ret;
}

#undef URL
#define URL API_URL"/sendMessage?chat_id=%"PRId64"&text=%s" \
            "&reply_to_message_id=%d&parse_mode=%s"

Message *
api_sendFormattedReply(Message *msg, const char *text, ParseMode parse_mode)
{
	int len;
	char *url, *enc_text;
	const char *parse_mode_str;
	json_object *json;
	Message *ret;

	parse_mode_str = (parse_mode == HTML) ? "HTML" : "Markdown";
	enc_text = encode_url(text);
	if (enc_text == NULL) {
		return NULL;
	}

	len = snprintf(NULL, 0, URL, msg->chat->id, enc_text, msg->message_id,
	               parse_mode_str);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len++;
	url = calloc(len, sizeof(char));
	if (url == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		free(enc_text);
		return NULL;
	}

	len = snprintf(url, len, URL, msg->chat->id, enc_text, msg->message_id,
	               parse_mode_str);
	free(enc_text);
	if (len < 0) {
		eprintf("snprintf failed: %s\n", strerror(errno));
		return NULL;
	}

	json = http_getjson(url);
	free(url);
	if (json == NULL) {
		return NULL;
	}

	ret = Message_parse(json);
	json_object_put(json);
	return ret;
}
