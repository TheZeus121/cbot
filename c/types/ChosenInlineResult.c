/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
ChosenInlineResult_free(ChosenInlineResult *cir)
{
	if (cir == NULL) {
		return;
	}

	FREE(cir->result_id, free);
	FREE(cir->from, User_free);
	FREE(cir->location, Location_free);
	FREE(cir->inline_message_id, free);
	FREE(cir->query, free);

	free(cir);
}

ChosenInlineResult *
ChosenInlineResult_parse(json_object *json)
{
	ChosenInlineResult *ret;
	json_object *jobj;

	ret = calloc(1, sizeof(ChosenInlineResult));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(result_id, string);
	MANDATORY(from, User);

	OPTIONAL3(location, Location, NULL);
	OPTIONAL3(inline_message_id, string, NULL);

	MANDATORY(query, string);

	return ret;

err:
	ChosenInlineResult_free(ret);
	return NULL;
}
