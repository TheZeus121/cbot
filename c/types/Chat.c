/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Chat_free(Chat *chat)
{
	if (chat == NULL) {
		return;
	}

	FREE(chat->type, free);
	FREE(chat->title, free);
	FREE(chat->username, free);
	FREE(chat->first_name, free);
	FREE(chat->last_name, free);
	FREE(chat->photo, ChatPhoto_free);
	FREE(chat->description, free);
	FREE(chat->invite_link, free);
	FREE(chat->pinned_message, Message_free);
	FREE(chat->sticker_set_name, free);

	free(chat);
}

Chat *
Chat_parse(json_object *json)
{
	json_object *jobj;
	Chat *ret;

	ret = calloc(1, sizeof(Chat));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(id,   int64);
	MANDATORY(type, string);

	OPTIONAL(title,      string);
	OPTIONAL(username,   string);
	OPTIONAL(first_name, string);
	OPTIONAL(last_name,  string);
	OPTIONAL3(all_members_are_administrators, boolean, false);
	OPTIONAL(photo,      ChatPhoto);
	OPTIONAL(description,         string);
	OPTIONAL(invite_link,         string);
	OPTIONAL(pinned_message,      Message);
	OPTIONAL(sticker_set_name,    string);
	OPTIONAL3(can_set_sticker_set, boolean, false);

	return ret;

err:
	Chat_free(ret);
	return NULL;
}
