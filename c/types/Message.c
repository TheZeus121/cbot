/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Message_free(Message *msg)
{
	if (msg == NULL) {
		return;
	}

	FREE(msg->from, User_free);
	FREE(msg->chat, Chat_free);
	FREE(msg->forward_from, User_free);
	FREE(msg->forward_from_chat, Chat_free);
	FREE(msg->forward_signature, free);
	FREE(msg->forward_sender_name, free);
	FREE(msg->reply_to_message, Message_free);
	FREE(msg->media_group_id, free);
	FREE(msg->author_signature, free);
	FREE(msg->text, free);
	FREE(msg->entities, MessageEntityArray_free);
	FREE(msg->caption_entities, MessageEntityArray_free);
	FREE(msg->audio, Audio_free);
	FREE(msg->document, Document_free);
	FREE(msg->animation, Animation_free);
	FREE(msg->game, Game_free);
	FREE(msg->photo, PhotoSizeArray_free);
	FREE(msg->sticker, Sticker_free);
	FREE(msg->video, Video_free);
	FREE(msg->voice, Voice_free);
	FREE(msg->video_note, VideoNote_free);
	FREE(msg->caption, free);
	FREE(msg->contact, Contact_free);
	FREE(msg->location, Location_free);
	FREE(msg->venue, Venue_free);
	FREE(msg->poll, Poll_free);
	FREE(msg->new_chat_members, UserArray_free);
	FREE(msg->left_chat_member, User_free);
	FREE(msg->new_chat_title, free);
	FREE(msg->new_chat_photo, PhotoSizeArray_free);
	FREE(msg->pinned_message, Message_free);
	FREE(msg->invoice, Invoice_free);
	FREE(msg->successful_payment, SuccessfulPayment_free);
	FREE(msg->connected_website, free);
	FREE(msg->passport_data, PassportData_free);

	free(msg);
}

Message *
Message_parse(json_object *json)
{
	json_object *jobj;
	Message *ret;

	ret = calloc(1, sizeof(Message));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(message_id, int);
	OPTIONAL3(from, User, NULL);
	MANDATORY(date, int);
	MANDATORY(chat, Chat);

	OPTIONAL3(forward_from, User, NULL);
	OPTIONAL3(forward_from_chat, Chat, NULL);
	OPTIONAL3(forward_from_message_id, int, -1);
	OPTIONAL3(forward_signature, string, NULL);
	OPTIONAL3(forward_sender_name, string, NULL);
	OPTIONAL3(forward_date, int, -1);
	OPTIONAL3(reply_to_message, Message, NULL);
	OPTIONAL3(edit_date, int, -1);
	OPTIONAL3(media_group_id, string, NULL);
	OPTIONAL3(author_signature, string, NULL);
	OPTIONAL3(text, string, NULL);
	OPTIONAL3(entities, MessageEntityArray, NULL);
	OPTIONAL3(caption_entities, MessageEntityArray, NULL);
	OPTIONAL3(audio, Audio, NULL);
	OPTIONAL3(document, Document, NULL);
	OPTIONAL3(animation, Animation, NULL);
	OPTIONAL3(game, Game, NULL);
	OPTIONAL3(photo, PhotoSizeArray, NULL);
	OPTIONAL3(sticker, Sticker, NULL);
	OPTIONAL3(video, Video, NULL);
	OPTIONAL3(voice, Voice, NULL);
	OPTIONAL3(video_note, VideoNote, NULL);
	OPTIONAL3(caption, string, NULL);
	OPTIONAL3(contact, Contact, NULL);
	OPTIONAL3(location, Location, NULL);
	OPTIONAL3(venue, Venue, NULL);
	OPTIONAL3(poll, Poll, NULL);
	OPTIONAL3(new_chat_members, UserArray, NULL);
	OPTIONAL3(left_chat_member, User, NULL);
	OPTIONAL3(new_chat_title, string, NULL);
	OPTIONAL3(new_chat_photo, PhotoSizeArray, NULL);
	OPTIONAL3(delete_chat_photo, boolean, false);
	OPTIONAL3(group_chat_created, boolean, false);
	OPTIONAL3(supergroup_chat_created, boolean, false);
	OPTIONAL3(channel_chat_created, boolean, false);
	OPTIONAL3(migrate_to_chat_id, int64, -1);
	OPTIONAL3(migrate_from_chat_id, int64, -1);
	OPTIONAL3(pinned_message, Message, NULL);
	OPTIONAL3(invoice, Invoice, NULL);
	OPTIONAL3(successful_payment, SuccessfulPayment, NULL);
	OPTIONAL3(connected_website, string, NULL);
	OPTIONAL3(passport_data, PassportData, NULL);

	return ret;

err:
	Message_free(ret);
	return NULL;
}
