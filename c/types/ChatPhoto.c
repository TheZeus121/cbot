/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
ChatPhoto_free(ChatPhoto *cp)
{
	if (cp == NULL) {
		return;
	}

	FREE(cp->small_file_id, free);
	FREE(cp->big_file_id, free);

	free(cp);
}

ChatPhoto *
ChatPhoto_parse(json_object *json)
{
	json_object *jobj;
	ChatPhoto *ret;

	ret = calloc(1, sizeof(ChatPhoto));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
	}

	MANDATORY(small_file_id, string);
	MANDATORY(big_file_id, string);

	return ret;

err:
	ChatPhoto_free(ret);
	return NULL;
}
