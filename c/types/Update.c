/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Update_free(Update *upd)
{
	if (upd == NULL) {
		return;
	}

	FREE(upd->message, Message_free);
	FREE(upd->edited_message, Message_free);
	FREE(upd->channel_post, Message_free);
	FREE(upd->edited_channel_post, Message_free);
	FREE(upd->inline_query, InlineQuery_free);
	FREE(upd->chosen_inline_result, ChosenInlineResult_free);
	FREE(upd->callback_query, CallbackQuery_free);
	FREE(upd->shipping_query, ShippingQuery_free);
	FREE(upd->pre_checkout_query, PreCheckoutQuery_free);
	FREE(upd->poll, Poll_free);

	free(upd);
}

Update *
Update_parse(json_object *json)
{
	json_object *jobj;
	Update *ret;

	ret = calloc(1, sizeof(Update));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(update_id, int);

	OPTIONAL3(message, Message, NULL);
	OPTIONAL3(edited_message, Message, NULL);
	OPTIONAL3(channel_post, Message, NULL);
	OPTIONAL3(edited_channel_post, Message, NULL);
	OPTIONAL3(inline_query, InlineQuery, NULL);
	OPTIONAL3(chosen_inline_result, ChosenInlineResult, NULL);
	OPTIONAL3(callback_query, CallbackQuery, NULL);
	OPTIONAL3(shipping_query, ShippingQuery, NULL);
	OPTIONAL3(pre_checkout_query, PreCheckoutQuery, NULL);
	OPTIONAL3(poll, Poll, NULL);

	return ret;

err:
	Update_free(ret);
	return NULL;
}
