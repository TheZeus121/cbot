/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
OrderInfo_free(OrderInfo *oi)
{
	if (oi == NULL) {
		return;
	}

	FREE(oi->name, free);
	FREE(oi->phone_number, free);
	FREE(oi->email, free);
	FREE(oi->shipping_address, ShippingAddress_free);

	free(oi);
}

OrderInfo *
OrderInfo_parse(json_object *json)
{
	json_object *jobj;
	OrderInfo *ret;

	ret = calloc(1, sizeof(OrderInfo));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	OPTIONAL3(name, string, NULL);
	OPTIONAL3(phone_number, string, NULL);
	OPTIONAL3(email, string, NULL);
	OPTIONAL3(shipping_address, ShippingAddress, NULL);

	return ret;

err:
	OrderInfo_free(ret);
	return NULL;
}
