/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Audio_free(Audio *audio)
{
	if (audio == NULL) {
		return;
	}

	FREE(audio->file_id, free);
	FREE(audio->performer, free);
	FREE(audio->title, free);
	FREE(audio->mime_type, free);
	FREE(audio->thumb, PhotoSize_free);

	free(audio);
}

Audio *
Audio_parse(json_object *json)
{
	Audio *ret;
	json_object *jobj;

	ret = calloc(1, sizeof(Audio));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(file_id, string);
	MANDATORY(duration, int);

	OPTIONAL3(performer, string, NULL);
	OPTIONAL3(title, string, NULL);
	OPTIONAL3(mime_type, string, NULL);
	OPTIONAL3(file_size, int, -1);
	OPTIONAL3(thumb, PhotoSize, NULL);

	return ret;

err:
	Audio_free(ret);
	return NULL;
}
