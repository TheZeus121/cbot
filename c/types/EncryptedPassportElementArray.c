/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"

void
EncryptedPassportElementArray_free(EncryptedPassportElement **epea)
{
	EncryptedPassportElement **idx;

	if (epea == NULL) {
		return;
	}

	for (idx = epea; *idx != NULL; idx++) {
		EncryptedPassportElement_free(*idx);
	}

	free(epea);
}

EncryptedPassportElement **
EncryptedPassportElementArray_parse(json_object *json)
{
	size_t i, len;
	EncryptedPassportElement **ret, *tmp;

	len = json_object_array_length(json);
	ret = calloc(len + 1, sizeof(EncryptedPassportElement *));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	for (i = 0; i < len; i++) {
		tmp = EncryptedPassportElement_parse(
			json_object_array_get_idx(json, i));
		if (tmp == NULL) {
			goto err;
		}

		ret[i] = tmp;
	}

	return ret;

err:
	EncryptedPassportElementArray_free(ret);
	return NULL;
}
