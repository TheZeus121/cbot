/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Contact_free(Contact *c)
{
	if (c == NULL) {
		return;
	}

	FREE(c->phone_number, free);
	FREE(c->first_name, free);
	FREE(c->last_name, free);
	FREE(c->vcard, free);

	free(c);
}

Contact *
Contact_parse(json_object *json)
{
	Contact *ret;
	json_object *jobj;

	ret = calloc(1, sizeof(Contact));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(phone_number, string);
	MANDATORY(first_name, string);

	OPTIONAL3(last_name, string, NULL);
	OPTIONAL3(user_id, int, -1);
	OPTIONAL3(vcard, string, NULL);

	return ret;

err:
	Contact_free(ret);
	return NULL;
}
