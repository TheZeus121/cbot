/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Game_free(Game *game)
{
	if (game == NULL) {
		return;
	}

	FREE(game->title, free);
	FREE(game->description, free);
	FREE(game->photo, PhotoSizeArray_free);
	FREE(game->text, free);
	FREE(game->text_entities, MessageEntityArray_free);
	FREE(game->animation, Animation_free);

	free(game);
}

Game *
Game_parse(json_object *json)
{
	Game *ret;
	json_object *jobj;

	ret = calloc(1, sizeof(Game));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(title, string);
	MANDATORY(description, string);
	MANDATORY(photo, PhotoSizeArray);

	OPTIONAL3(text, string, NULL);
	OPTIONAL3(text_entities, MessageEntityArray, NULL);
	OPTIONAL3(animation, Animation, NULL);

	return ret;

err:
	Game_free(ret);
	return NULL;
}
