/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
EncryptedPassportElement_free(EncryptedPassportElement *epe)
{
	if (epe == NULL) {
		return;
	}

	FREE(epe->type, free);
	FREE(epe->data, free);
	FREE(epe->phone_number, free);
	FREE(epe->email, free);
	FREE(epe->files, PassportFileArray_free);
	FREE(epe->front_side, PassportFile_free);
	FREE(epe->reverse_side, PassportFile_free);
	FREE(epe->selfie, PassportFile_free);
	FREE(epe->translation, PassportFileArray_free);
	FREE(epe->hash, free);

	free(epe);
}

EncryptedPassportElement *
EncryptedPassportElement_parse(json_object *json)
{
	EncryptedPassportElement *ret;
	json_object *jobj;

	ret = calloc(1, sizeof(EncryptedPassportElement));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(type, string);

	OPTIONAL3(data, string, NULL);
	OPTIONAL3(phone_number, string, NULL);
	OPTIONAL3(email, string, NULL);
	OPTIONAL3(files, PassportFileArray, NULL);
	OPTIONAL3(front_side, PassportFile, NULL);
	OPTIONAL3(reverse_side, PassportFile, NULL);
	OPTIONAL3(selfie, PassportFile, NULL);
	OPTIONAL3(translation, PassportFileArray, NULL);

	MANDATORY(hash, string);

	return ret;

err:
	EncryptedPassportElement_free(ret);
	return NULL;
}
