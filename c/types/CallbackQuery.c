/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
CallbackQuery_free(CallbackQuery *cbq)
{
	if (cbq == NULL) {
		return;
	}

	FREE(cbq->id, free);
	FREE(cbq->from, User_free);
	FREE(cbq->message, Message_free);
	FREE(cbq->inline_message_id, free);
	FREE(cbq->chat_instance, free);
	FREE(cbq->data, free);
	FREE(cbq->game_short_name, free);

	free(cbq);
}

CallbackQuery *
CallbackQuery_parse(json_object *json)
{
	CallbackQuery *ret;
	json_object *jobj;

	ret = calloc(1, sizeof(CallbackQuery));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(id, string);
	MANDATORY(from, User);

	OPTIONAL3(message, Message, NULL);
	OPTIONAL3(inline_message_id, string, NULL);

	MANDATORY(chat_instance, string);

	OPTIONAL3(data, string, NULL);
	OPTIONAL3(game_short_name, string, NULL);

	return ret;

err:
	CallbackQuery_free(ret);
	return NULL;
}
