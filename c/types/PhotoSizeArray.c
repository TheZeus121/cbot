/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"

void
PhotoSizeArray_free(PhotoSize **psa)
{
	PhotoSize **idx;

	if (psa == NULL) {
		return;
	}

	for (idx = psa; *idx != NULL; idx++) {
		PhotoSize_free(*idx);
	}

	free(psa);
}

PhotoSize **
PhotoSizeArray_parse(json_object *json)
{
	size_t i, len;
	PhotoSize **ret;
	PhotoSize  *tmp;

	len = json_object_array_length(json);
	ret = calloc(len + 1, sizeof(PhotoSize *));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	for (i = 0; i < len; i++) {
		tmp = PhotoSize_parse(json_object_array_get_idx(json, i));
		if (tmp == NULL) {
			goto err;
		}

		ret[i] = tmp;
	}

	return ret;

err:
	PhotoSizeArray_free(ret);
	return NULL;
}
