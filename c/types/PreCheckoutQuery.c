/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
PreCheckoutQuery_free(PreCheckoutQuery *pcq)
{
	if (pcq == NULL) {
		return;
	}

	FREE(pcq->id, free);
	FREE(pcq->from, User_free);
	FREE(pcq->currency, free);
	FREE(pcq->invoice_payload, free);
	FREE(pcq->shipping_option_id, free);
	FREE(pcq->order_info, OrderInfo_free);

	free(pcq);
}

PreCheckoutQuery *
PreCheckoutQuery_parse(json_object *json)
{
	json_object *jobj;
	PreCheckoutQuery *ret;

	ret = calloc(1, sizeof(PreCheckoutQuery));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(id, string);
	MANDATORY(from, User);
	MANDATORY(currency, string);
	MANDATORY(total_amount, int);
	MANDATORY(invoice_payload, string);

	OPTIONAL3(shipping_option_id, string, NULL);
	OPTIONAL3(order_info, OrderInfo, NULL);

	return ret;

err:
	PreCheckoutQuery_free(ret);
	return NULL;
}
