/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
User_free(User *usr)
{
	if (usr == NULL) {
		return;
	}

	FREE(usr->first_name, free);
	FREE(usr->last_name, free);
	FREE(usr->username, free);
	FREE(usr->language_code, free);

	free(usr);
}

User *
User_parse(json_object *json)
{
	json_object *jobj;
	User *ret;

	ret = calloc(1, sizeof(User));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(id, int);
	MANDATORY(is_bot, boolean);
	MANDATORY(first_name, string);

	OPTIONAL(last_name, string);
	OPTIONAL(username, string);
	OPTIONAL(language_code, string);

	return ret;

err:
	User_free(ret);
	return NULL;
}
