/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
ShippingAddress_free(ShippingAddress *sa)
{
	if (sa == NULL) {
		return;
	}

	FREE(sa->country_code, free);
	FREE(sa->state, free);
	FREE(sa->city, free);
	FREE(sa->street_line1, free);
	FREE(sa->street_line2, free);
	FREE(sa->post_code, free);

	free(sa);
}

ShippingAddress *
ShippingAddress_parse(json_object *json)
{
	json_object *jobj;
	ShippingAddress *ret;

	ret = calloc(1, sizeof(ShippingAddress));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(country_code, string);
	MANDATORY(state, string);
	MANDATORY(city, string);
	MANDATORY(street_line1, string);
	MANDATORY(street_line2, string);
	MANDATORY(post_code, string);

	return ret;

err:
	ShippingAddress_free(ret);
	return ret;
}
