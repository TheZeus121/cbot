/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Poll_free(Poll *p)
{
	if (p == NULL) {
		return;
	}

	FREE(p->id, free);
	FREE(p->question, free);
	FREE(p->options, PollOptionArray_free);

	free(p);
}

Poll *
Poll_parse(json_object *json)
{
	json_object *jobj;
	Poll *ret;

	ret = calloc(1, sizeof(Poll));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(id, string);
	MANDATORY(question, string);
	MANDATORY(options, PollOptionArray);
	MANDATORY(is_closed, boolean);

	return ret;

err:
	Poll_free(ret);
	return NULL;
}
