/* Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "parse.h"

void
Venue_free(Venue *ven)
{
	if (ven == NULL) {
		return;
	}

	FREE(ven->location, Location_free);
	FREE(ven->title, free);
	FREE(ven->address, free);
	FREE(ven->foursquare_id, free);
	FREE(ven->foursquare_type, free);

	free(ven);
}

Venue *
Venue_parse(json_object *json)
{
	json_object *jobj;
	Venue *ret;

	ret = calloc(1, sizeof(Venue));
	if (ret == NULL) {
		eprintf("calloc failed: %s\n", strerror(errno));
		return NULL;
	}

	MANDATORY(location, Location);
	MANDATORY(title, string);
	MANDATORY(address, string);

	OPTIONAL3(foursquare_id, string, NULL);
	OPTIONAL3(foursquare_type, string, NULL);

	return ret;

err:
	Venue_free(ret);
	return NULL;
}
