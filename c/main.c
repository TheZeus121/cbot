/* main.c -- Entry point into cbot.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <curl/curl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "cbot.h"
#include "config.h"
#include "unicode_util.h"

#define START_BANNER \
BOTNAME"  Copyright (C) 2019  Uko Koknevics\n\n" \
"This bot comes with <i>ABSOLUTELY NO WARRANTY</i>; for details send " \
"/warranty.\n\n" \
"<a href=\""SRC_URL"\">Source code</a>."

#define WARRANTY_BANNER \
"This bot comes with <i>ABSOLUTELY NO WARRANTY</i>.  The following " \
"sections from the GNU Affero General Public License, version 3, should " \
"make that clear.\n\n" \
"  <b>15. Disclaimer of Warranty.</b>\n\n" \
"  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY " \
"APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT " \
"HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT " \
"WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT " \
"LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR " \
"A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND " \
"PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE " \
"DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR " \
"CORRECTION.\n\n" \
"  <b>16. Limitation of Liability.</b>\n\n" \
"  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN " \
"WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES " \
"AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR " \
"DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL " \
"DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM " \
"(INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED " \
"INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF " \
"THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER " \
"OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH " \
"DAMAGES.\n\n" \
"  <b>17. Interpretation of Sections 15 and 16.</b>\n\n" \
"  If the disclaimer of warranty and limitation of liability provided " \
"above cannot be given local legal effect according to their terms, " \
"reviewing courts shall apply local law that most closely approximates " \
"an absolute waiver of all civil liability in connection with the " \
"Program, unless a warranty or assumption of liability accompanies a " \
"copy of the Program in return for a fee.\n\n" \
"See &lt;https://www.gnu.org/licenses/agpl.html&gt; for more details."

static bool running = true;

static UChar *U_USERNAME;

/* TODO: find a better way */
U_STRING_DECL(str_at_sign, "@", 1);
U_STRING_DECL(str_bot_command, "bot_command", 11);
U_STRING_DECL(str_ping, "ping", 4);
U_STRING_DECL(str_shutdown, "shutdown", 8);
U_STRING_DECL(str_start, "start", 5);
U_STRING_DECL(str_warranty, "warranty", 8);

static bool
init_username(void)
{
	int32_t cap;
	UErrorCode err;

	err = U_ZERO_ERROR;
	u_strFromUTF8Lenient(NULL, 0, &cap, USERNAME, -1, &err);
	if (U_FAILURE(err) && err != U_BUFFER_OVERFLOW_ERROR) {
		eprintf("Unicode error: %s\n", u_errorName(err));
		return false;
	}

	U_USERNAME = calloc(cap + 1, sizeof(UChar));
	if (U_USERNAME == NULL) {
		eputs("Allocation error");
		return false;
	}

	err = U_ZERO_ERROR;
	u_strFromUTF8Lenient(U_USERNAME, cap, NULL, USERNAME, -1, &err);
	if (U_FAILURE(err)) {
		free(U_USERNAME);
		eprintf("Unicode error: %s\n", u_errorName(err));
		return false;
	}

	return true;
}

static void
process_message(Message *msg)
{
	UChar *command, *cmd, *uname, *state;
	Message *reply;
	MessageEntity **idx, *entity;

	if (msg->entities == NULL || msg->text == NULL) {
		return;
	}

	for (idx = msg->entities; *idx != NULL; idx++) {
		entity = *idx;
		if (u_strcmp(entity->type, str_bot_command) != 0) {
			continue;
		}

		if (entity->offset != 0) {
			continue;
		}

		command = u_strndup(msg->text, entity->length);
		cmd = u_strtok_r(command, str_at_sign, &state);
		uname = u_strtok_r(NULL, str_at_sign, &state);
		if (uname != NULL
		    && (u_strcmp(uname, U_USERNAME) != 0
		        || u_strtok_r(NULL, str_at_sign, &state) != NULL)) {
			free(command);
			continue;
		}

		cmd++;

		if (u_strcmp(cmd, str_ping) == 0) {
			reply = api_sendReply(msg, "Pong!");
			Message_free(reply);
		} else if (u_strcmp(cmd, str_shutdown) == 0) {
			if (msg->from != NULL && msg->from->id == OWNER) {
				reply = api_sendReply(msg, "Shutting down...");
				Message_free(reply);
				running = false;
			}
		} else if (u_strcmp(cmd, str_start) == 0) {
			reply = api_sendFormattedReply(msg, START_BANNER, HTML);
			Message_free(reply);
		} else if (u_strcmp(cmd, str_warranty) == 0) {
			reply = api_sendFormattedReply(msg, WARRANTY_BANNER,
			                               HTML);
			Message_free(reply);
		}

		free(command);
	}
}

int
main(void)
{
	int offset;
	Update **upds, **idx, *upd;

	U_STRING_INIT(str_at_sign, "@", 1);
	U_STRING_INIT(str_bot_command, "bot_command", 11);
	U_STRING_INIT(str_ping, "ping", 4);
	U_STRING_INIT(str_shutdown, "shutdown", 8);
	U_STRING_INIT(str_start, "start", 5);
	U_STRING_INIT(str_warranty, "warranty", 8);

	if (!init_username()) {
		return EXIT_FAILURE;
	}

	if (curl_global_init(CURL_GLOBAL_ALL) != 0) {
		eputs("Cannot initialize CURL, quitting...");
		return -1;
	}

	offset = 0;
	while (running) {
		upds = api_getUpdates(offset);
		if (upds == NULL) {
			continue;
		}

		for (idx = upds; *idx != NULL; idx++) {
			upd = *idx;
			offset = upd->update_id + 1;
			if (upd->message) {
				process_message(upd->message);
			}
		}

		UpdateArray_free(upds);
	}

	/* So that the processed updates don't come back the next
	 * time. */
	upds = api_getUpdatesShortPoll(offset);
	UpdateArray_free(upds);
	curl_global_cleanup();
	return 0;
}
