/* api.h -- Header include to expose Telegram API types & methods.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <json_object.h>

#include "api_types.h"

/* FIXME: use Unicode here as well. */

typedef enum {
	HTML,
	MARKDOWN,
} ParseMode;

/* /getMe */
User *api_getMe(void);

/* /getUpdates */
Update **api_getUpdates(int offset);
Update **api_getUpdatesShortPoll(int offset);

/* /sendMessage */
Message *api_sendFormattedMessage(int64_t chat_id, const char *text,
                                  ParseMode parse_mode);
Message *api_sendFormattedReply(Message *msg, const char *text,
                                ParseMode parse_mode);
Message *api_sendMessage(int64_t chat_id, const char *text);
Message *api_sendReply(Message *msg, const char *text);
