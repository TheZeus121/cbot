/* cbot.h -- Header exposing common includes.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <json_object.h>

#define eprintf(fmt, ...) fprintf(stderr, "E %s: "fmt, __func__, __VA_ARGS__)
#define eputs(s) fprintf(stderr, "E %s: %s\n", __func__, s)

json_object *http_getjson(const char *url);
char        *http_getstr(const char *url);

char *encode_url(const char *str);
