/* api_types.h -- Header exposing Telegram API types.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <unicode/ustring.h>

typedef struct Animation_                Animation;
typedef struct Audio_                    Audio;
typedef struct CallbackQuery_            CallbackQuery;
typedef struct Chat_                     Chat;
typedef struct ChatPhoto_                ChatPhoto;
typedef struct ChosenInlineResult_       ChosenInlineResult;
typedef struct Contact_                  Contact;
typedef struct Document_                 Document;
typedef struct EncryptedCredentials_     EncryptedCredentials;
typedef struct EncryptedPassportElement_ EncryptedPassportElement;
typedef struct Game_                     Game;
typedef struct InlineQuery_              InlineQuery;
typedef struct Invoice_                  Invoice;
typedef struct Location_                 Location;
typedef struct MaskPosition_             MaskPosition;
typedef struct Message_                  Message;
typedef struct MessageEntity_            MessageEntity;
typedef struct OrderInfo_                OrderInfo;
typedef struct PassportData_             PassportData;
typedef struct PassportFile_             PassportFile;
typedef struct PhotoSize_                PhotoSize;
typedef struct Poll_                     Poll;
typedef struct PollOption_               PollOption;
typedef struct PreCheckoutQuery_         PreCheckoutQuery;
typedef struct ShippingAddress_          ShippingAddress;
typedef struct ShippingQuery_            ShippingQuery;
typedef struct Sticker_                  Sticker;
typedef struct SuccessfulPayment_        SuccessfulPayment;
typedef struct Update_                   Update;
typedef struct User_                     User;
typedef struct Venue_                    Venue;
typedef struct Video_                    Video;
typedef struct VideoNote_                VideoNote;
typedef struct Voice_                    Voice;

/*! This object represents an animation file (GIF or H.264/MPEG-4 AVC
 *  video without sound). */
struct Animation_ {
	/*! Unique file identifier. */
	UChar *file_id;
	/*! Video width as defined by sender. */
	int   width;
	/*! Video height as defined by sender. */
	int   height;
	/*! Duration of the video in seconds as defined by sender. */
	int   duration;
	/*! Optional.  Animation thumbnail as defined by sender. */
	PhotoSize *thumb;
	/*! Optional.  Original animation filename as defined by
	 *  sender. */
	UChar *file_name;
	/*! Optional.  MIME type of the file as defined by sender. */
	UChar *mime_type;
	/*! Optional.  File size. */
	int   file_size;
};

/*! This object represents an audio file to be treated as music by the
 *  Telegram clients. */
struct Audio_ {
	/*! Unique identifier for this file. */
	UChar *file_id;
	/*! Duration of the audio in seconds as defined by sender. */
	int   duration;
	/*! Optional.  Performer of the audio as defined by sender or
	 *  by audio tags. */
	UChar *performer;
	/*! Optional.  Title of the audio as defined by sender or by
	 *  audio tags. */
	UChar *title;
	/*! Optional.  MIME type of the file as defined by sender. */
	UChar *mime_type;
	/*! Optional.  File size. */
	int   file_size;
	/*! Optional.  Thumbnail of the album cover to which the music
	 *  file belogs. */
	PhotoSize *thumb;
};

/*! This object represents an incoming callback query from a callback
 *  button in an inline keyboard. */
struct CallbackQuery_ {
	/*! Unique identifier for this query. */
	UChar    *id;
	/*! Sender. */
	User    *from;
	/*! Optional.  Message with the callback button that
	 *  originated the query. */
	Message *message;
	/*! Optional.  Identifier of the message sent via the bot in
	 *  inline mode, that originated the query. */
	UChar    *inline_message_id;
	/*! Global identifier, uniquely corresponding to the chat to
	 *  which the message with the callback button was sent. */
	UChar    *chat_instance;
	/*! Optional.  Data associated with the callback button. */
	UChar    *data;
	/*! Optional.  Short name of a Game to be returned, serves as
	 *  the unique identifier for the game. */
	UChar    *game_short_name;
};

/*! This object represents a chat. */
struct Chat_ {
	/*! Unique identifier for this chat. */
	int64_t    id;
	/*! Type of chat, can be either "private", "group",
	 *  "supergroup", or "channel" */
	UChar      *type;
	/*! Optional.  Title, for supergroups, channels, and group
	 *  chats. */
	UChar      *title;
	/*! Optional.  Username, for private chats, supergroups, and
	 *  channels if available */
	UChar      *username;
	/*! Optional.  First name of the other party in a private
	 *  chat. */
	UChar      *first_name;
	/*! Optional.  Last name of the other party in a private
	 *  chat. */
	UChar      *last_name;
	/*! Optional.  True if a group has 'All Members Are Admins'
	 *  enabled. */
	bool       all_members_are_administrators;
	/*! Optional.  Chat photo. */
	ChatPhoto *photo;
	/*! Optional.  Description, for supergroups and channel
	 *  chats. */
	UChar      *description;
	/*! Optional.  Chat invite link, for supergroups and channel
	 *  chats. */
	UChar      *invite_link;
	/*! Optional.  Pinned message, for groups, supergroups, and
	 *  channels. */
	Message   *pinned_message;
	/*! Optional.  For supergroups, name of group sticker set. */
	UChar      *sticker_set_name;
	/*! Optional.  True, if the bot can change the group sticker
	 *  set. */
	bool       can_set_sticker_set;
};

/*! This object represents a chat photo. */
struct ChatPhoto_ {
	/*! Unique file identifier of small (160x160) chat photo. */
	UChar *small_file_id;
	/*! Unique file identifier of big (640x640) chat photo. */
	UChar *big_file_id;
};

/*! Represents a result of an inline query that was chosen by the user
 *  and sent to their chat partner. */
struct ChosenInlineResult_ {
	/*! The unique identifier for the result that was chosen. */
	UChar     *result_id;
	/*! The user that chose the result. */
	User     *from;
	/*! Optional.  Sender location, only for bots that require
	 *  user location. */
	Location *location;
	/*! Optional.  Identifier of the sent inline message. */
	UChar     *inline_message_id;
	/*! The query that was used to obtain the result. */
	UChar     *query;
};

/*! This object represents a phone contact. */
struct Contact_ {
	/*! Contact's phone number. */
	UChar *phone_number;
	/*! Contact's first name. */
	UChar *first_name;
	/*! Optional.  Contact's last name. */
	UChar *last_name;
	/*! Optional.  Contact's user identifier in Telegram. */
	int   user_id;
	/*! Optional.  Additional data about the contact in the form
	 *  of a vCard. */
	UChar *vcard;
};

/*! This object represents a general file (as opposed to photos, voice
 *  messages, audio files, &c.) */
struct Document_ {
	/*! Unique file identifier. */
	UChar *file_id;
	/*! Optional.  Document thumbnail as defined by sender. */
	PhotoSize *thumb;
	/*! Optional.  Original filename as defined by sender. */
	UChar *file_name;
	/*! Optional.  MIME type of the file as defined by sender. */
	UChar *mime_type;
	/*! Optional.  File size. */
	int   file_size;
};

/*! Contains data required for decrypting and authenticating
 *  EncryptedPassportElement. */
struct EncryptedCredentials_ {
	/*! Base64-encoded encrypted JSON-serialized data with unique
	 *  user's payload, data hashes, and secrets required for
	 *  EncryptedPassportElement decryption and authentication. */
	UChar *data;
	/*! Base64-encoded data has for data authentication. */
	UChar *hash;
	/*! Base64-encoded secret, encrypted with the bot's public RSA
	 *  key, required for data decryption. */
	UChar *secret;
};

/*! Contains information about documents or other Telegram Passport
 *  elements shared with the bot by the user. */
struct EncryptedPassportElement_ {
	/*! Element type.  One of "personal_details", "passport",
	 *  "driver_license", "identity_card", "internal_passport",
	 *  "address", "utility_bill", "bank_statement",
	 *  "rental_agreement", "passport_registration",
	 *  "temporary_registration", "phone_number", "email". */
	UChar *type;
	/*! Optional.  Base64-encoded encrypted Telegram Passport
	 *  element data provided by the user, available for
	 *  "personal_details", "passport", "driver_license",
	 *  "identity_card", "internal_passport", and "address"
	 *  types. */
	UChar *data;
	/*! Optional.  User's verified phone number, available only
	 *  for "phone_number" type. */
	UChar *phone_number;
	/*! Optional.  User's verified email address, available only
	 *  for "email" type. */
	UChar *email;
	/*! Optional.  Array of encrypted files with documents
	 *  provided by the user, available for "utility_bill",
	 *  "bank_statement", "rental_agreement",
	 *  "passport_registration", and "temporary_registration"
	 *  types. */
	PassportFile **files;
	/*! Optional.  Encrypted file with the front side of the
	 *  document, provided by the user.  Available for "passport",
	 *  "driver_license", "identity_card", and
	 *  "internal_passport". */
	PassportFile *front_side;
	/*! Optional.  Encrypted file with the reverse side of the
	 *  document, provided by the user.  Available for
	 *  "driver_license" and "identity_card". */
	PassportFile *reverse_side;
	/*! Optional.  Encrypted file with the selfie of the user
	 *  holding a document, provided by the user; available for
	 *  "passport", "driver_license", "identity_card", and
	 *  "internal_passport". */
	PassportFile *selfie;
	/*! Optional.  Array of encrypted files with translated
	 *  versions of documents provided by the user.  Available if
	 *  requested for "passport", "driver_license",
	 *  "identity_card", "internal_passport", "utility_bill",
	 *  "bank_statement", "rental_agreement",
	 *  "passport_registration", and "temporary_registration"
	 *  types. */
	PassportFile **translation;
	/*! Base64-encoded element hash for using in
	 *  PassportElementErrorUnspecified. */
	UChar *hash;
};

/*! This object represents a game. */
struct Game_ {
	/*! Title of the string. */
	UChar *title;
	/*! Description of the game. */
	UChar *description;
	/*! Photo that will be displayed in the game message in
	 *  chats. */
	PhotoSize **photo;
	/*! Optional.  Brief description of the game or high scores
	 *  included in the game message.  0-4096 characters. */
	UChar *text;
	/*! Optional.  Special entities that appear in text. */
	MessageEntity **text_entities;
	/*! Optional.  Animation that will be displayed in the game
	 *  message in chats. */
	Animation *animation;
};

/*! This object represents an incoming inline query. */
struct InlineQuery_ {
	/*! Unique identifier for this query. */
	UChar     *id;
	/*! Sender. */
	User     *from;
	/*! Optional.  Sender location, only for bots that request
	 *  user location. */
	Location *location;
	/*! Text of the query (up to 512 characters). */
	UChar     *query;
	/*! Offset of the results to be returned, can be controlled by
	 *  the bot. */
	UChar     *offset;
};

/*! This object contains basic information about an invoice. */
struct Invoice_ {
	/*! Product name. */
	UChar *title;
	/*! Product description. */
	UChar *description;
	/*! Unique bot deep-linking parameter that can be used to
	 *  generate this invoice. */
	UChar *start_parameter;
	/*! Three-letter ISO 4217 currency code. */
	UChar *currency;
	/*! Total price in the smallest units of the currency. */
	int   total_amount;
};

/*! This object represents a point on the map. */
struct Location_ {
	/*! Longitude as defined by the sender. */
	double longitude;
	/*! Latitude as defined by the sender. */
	double latitude;
};

/*! This object describes the position on faces where a mask should be
 *  placed by default. */
struct MaskPosition_ {
	/*! The part of the face relative to which the mask should be
	 *  placed.  One of "forehead", "eyes", "mouth", or "chin". */
	UChar  *point;
	/*! Shift by X-axis measured in widths of the mask scaled to
	 *  the face size, from left to right. */
	double x_shift;
	/*! Shift by Y-axis measured in heights of the mask scaled to
	 *  the face size, from top to bottom. */
	double y_shift;
	/*! Mask scaling coefficient. */
	double scale;
};

/*! This object represents a message. */
struct Message_ {
	/*! Unique message identifier inside the chat. */
	int           message_id;
	/*! Optional.  Sender, empty for messages sent to channels. */
	User         *from;
	/*! Date the message was sent in Unix time. */
	int           date;
	/*! Conversation the message belongs to. */
	Chat         *chat;
	/*! Optional.  For forwarded messages, sender of the original
	 *  message. */
	User         *forward_from;
	/*! Optional.  For messages forwarded from channels,
	 *  informaton about the original channel. */
	Chat         *forward_from_chat;
	/*! Optional.  For messages forwarded from channels,
	 *  identifier of the original message in the channel. */
	int           forward_from_message_id;
	/*! Optional.  For messages forwarded from channels, signature
	 *  of the post author, if present. */
	UChar         *forward_signature;
	/*! Optional.  For messages forwarded from from users who
	 *  disallow adding a link to their account in forwarded
	 *  messages. */
	UChar         *forward_sender_name;
	/*! Optional.  For forwarded messages, date the original
	 *  message was sent in Unix time. */
	int           forward_date;
	/*! Optional.  For replies, the original message. */
	Message      *reply_to_message;
	/*! Optional.  Date the message was last edited in Unix time. */
	int           edit_date;
	/*! Optional.  The unique identifier of a media message group
	 *  this message belongs to. */
	UChar         *media_group_id;
	/*! Optional.  Signature of the post author for messages in
	 *  channels. */
	UChar         *author_signature;
	/*! Optional.  For text messages, the actual UTF-8 text of the
	 *  message, 0-4096 characters. */
	UChar         *text;
	/*! Optional.  For text messages, special entities like
	 *  usernames, URLs, bot commands, &c. that appear in the
	 *  text. */
	MessageEntity **entities;
	/*! Optional.  For messages with a caption, special entities
	 *  that appear in the caption. */
	MessageEntity **caption_entities;
	/*! Optional.  Message is an audio file, information about the
	 *  file. */
	Audio        *audio;
	/*! Optional.  Message is a general file, information about
	 *  the file. */
	Document     *document;
	/*! Optional.  Message is an animation, information about the
	 *  animation. */
	Animation    *animation;
	/*! Optional.  Message is a game, information about the
	 *  game. */
	Game         *game;
	/*! Optional.  Message is a photo, available sizes of the
	 *  photo. */
	PhotoSize   **photo;
	/*! Optional.  Message is a sticker, information about the
	 *  sticker. */
	Sticker      *sticker;
	/*! Optional.  Message is a video, information about the
	 *  video. */
	Video        *video;
	/*! Optional.  Message is a voice message, information about
	 *  the file. */
	Voice        *voice;
	/*! Optional.  Message is a video note, information about the
	 *  video message. */
	VideoNote    *video_note;
	/*! Optional.  Caption for the animation, audio, document,
	 *  photo, video, or voice, 0-1024 characters. */
	UChar         *caption;
	/*! Optional.  Message is a shared contact, information about
	 *  the contact. */
	Contact      *contact;
	/*! Optional.  Message is a shared location, information about
	 *  the location. */
	Location     *location;
	/*! Optional.  Message is a venue, information about the
	 *  venue. */
	Venue        *venue;
	/*! Optional.  Message is a native poll, information about the
	 *  poll. */
	Poll         *poll;
	/*! Optional.  New members that were added to the group or
	 *  supergroup and information about them. */
	User        **new_chat_members;
	/*! Optional.  A member was removed from the group,
	 *  information about them. */
	User         *left_chat_member;
	/*! Optional.  A chat title was changed to this value. */
	UChar         *new_chat_title;
	/*! Optional.  A chat photo was changed to this value. */
	PhotoSize   **new_chat_photo;
	/*! Optional.  Service message: the chat photo was deleted. */
	bool          delete_chat_photo;
	/*! Optional.  Service message: the group has been created. */
	bool          group_chat_created;
	/*! Optional.  Service message: the supergroup has been
	 *  created. */
	bool          supergroup_chat_created;
	/*! Optional.  Service message: the channel has been
	 *  created. */
	bool          channel_chat_created;
	/*! Optional.  The group has been migrated to a supergroup
	 *  with the specified identifier. */
	int64_t       migrate_to_chat_id;
	/*! Optional.  The supergroup has been migrated from a group
	 *  with the specified identifier. */
	int64_t       migrate_from_chat_id;
	/*! Optional.  Specified message was pinned. */
	Message      *pinned_message;
	/*! Optional.  Message is an invoice for a payment,
	 *  information about the invoice. */
	Invoice      *invoice;
	/*! Optional.  Message is a service message about a successful
	 *  payment. */
	SuccessfulPayment *successful_payment;
	/*! Optional.  The domain name of the website on which the
	 *  user has logged in. */
	UChar         *connected_website;
	/*! Optional.  Telegram Passport data. */
	PassportData *passport_data;
};

/*! This object represents one special entity in a text message. */
struct MessageEntity_ {
	/*! Type of the entity.  Can be mention, hashtag, cashtag,
	 *  bot_command, url, email, phone_number, bold, italic, code,
	 *  pre, text_link, text_mention */
	UChar *type;
	/*! Offset in UTF-16 code units to the start of the entity. */
	int   offset;
	/*! Length of the entity in UTF-16 code units. */
	int   length;
	/*! Optional.  For "text_link" only, url that will be opened
	 *  after user taps on the text. */
	UChar *url;
	/*! Optional.  Fot "text_mention" only, the mentioned user. */
	User *user;
};

/*! This object represents information about an order. */
struct OrderInfo_ {
	/*! Optional.  User's name. */
	UChar *name;
	/*! Optional.  User's phone number. */
	UChar *phone_number;
	/*! Optional.  User's email. */
	UChar *email;
	/*! Optional.  User's shipping address. */
	ShippingAddress *shipping_address;
};

/*! Contains information about Telegram Passport data shared with the
 *  bot by the user. */
struct PassportData_ {
	/*! Array with information about documents and other Telegram
	 *  Passport elements that was shared with the bot. */
	EncryptedPassportElement **data;
	/*! Encrypted credentials required to decrypt the data. */
	EncryptedCredentials      *credentials;
};

/*! This object represents a file uploaded to Telegram Passport. */
struct PassportFile_ {
	/*! Unique identifier for this file. */
	UChar *file_id;
	/*! File size. */
	int   file_size;
	/*! Unix time when the file was uploaded. */
	int   file_date;
};

/*! This object represents one size of a photo or a file/sticker
 *  thumbnail. */
struct PhotoSize_ {
	/*! Unique identifier for this file. */
	UChar *file_id;
	/*! Photo width. */
	int   width;
	/*! Photo height. */
	int   height;
	/*! Optional.  File size. */
	int   file_size;
};

/*! This object contains information about a poll. */
struct Poll_ {
	/*! Unique poll identifier. */
	UChar *id;
	/*! Poll question, 1-255 characters. */
	UChar *question;
	/*! List of poll options. */
	PollOption **options;
	/*! True, if the poll is closed. */
	bool  is_closed;
};

/*! This object contains information about one answer in a poll. */
struct PollOption_ {
	/*! Option text, 1-100 characters. */
	UChar *text;
	/*! Number of users that voted for this option. */
	int   voter_count;
};

/*! This object contains information about an incoming pre-checkout
 *  query. */
struct PreCheckoutQuery_ {
	/*! Unique query identifier. */
	UChar *id;
	/*! User who sent the query. */
	User *from;
	/*! Three-letter ISO 4217 currency code. */
	UChar *currency;
	/*! Total price in the smallest units of the currency. */
	int   total_amount;
	/*! Bot specified invoice payload. */
	UChar *invoice_payload;
	/*! Optional.  Identifier of the shipping option chosen by the
	 *  user. */
	UChar *shipping_option_id;
	/*! Optional.  Order info provided by the user. */
	OrderInfo *order_info;
};

/*! This object represents a shipping address. */
struct ShippingAddress_ {
	/*! ISO 3166-1 alpha-2 country code. */
	UChar *country_code;
	/*! State, if applicable. */
	UChar *state;
	/*! City */
	UChar *city;
	/*! First line for the address. */
	UChar *street_line1;
	/*! Second line for the address. */
	UChar *street_line2;
	/*! Address post code. */
	UChar *post_code;
};

/*! This object contains information about an incoming shipping
 *  query. */
struct ShippingQuery_ {
	/*! Unique query identifier. */
	UChar *id;
	/*! User who sent the query. */
	User *from;
	/*! Bot specified invoice payload. */
	UChar *invoice_payload;
	/*! User specified shipping address. */
	ShippingAddress *shipping_address;
};

/*! This object represents a sticker. */
struct Sticker_ {
	/*! Unique identifier for this file. */
	UChar *file_id;
	/*! Sticker width */
	int   width;
	/*! Sticker height */
	int   height;
	/*! Optional.  Sticker thumbnail in the .webp or .jpg
	 *  format. */
	PhotoSize    *thumb;
	/*! Optional.  Emoji associated with the sticker. */
	UChar *emoji;
	/*! Optional.  Name of the sticker set to which the sticker
	 *  belongs. */
	UChar *set_name;
	/*! Optional.  For mask stickers, the position where the mask
	 *  should be placed. */
	MaskPosition *mask_position;
	/*! Optional.  File size. */
	int   file_size;
};

/*! This object contains basic information about a successful
 *  payment. */
struct SuccessfulPayment_ {
	/*! Three-letter ISO 4217 currency code. */
	UChar *currency;
	/*! Total price in the smallest units of the currency. */
	int   total_amount;
	/*! Bot specified invoice payload. */
	UChar *invoice_payload;
	/*! Optional.  Identifier of the shipping option chosen by the
	 *  user. */
	UChar *shipping_option_id;
	/*! Optional.  Order info provided by the user. */
	OrderInfo *order_info;
	/*! Telegram payment identifier. */
	UChar *telegram_payment_charge_id;
	/*! Provider payment identifier. */
	UChar *provider_payment_charge_id;
};

/*! This object represents an incoming update. */
struct Update_ {
	/*! The update's unique identifier */
	int               update_id;
	/*! Optional.  New incoming message of any kind. */
	Message          *message;
	/*! Optional.  New version of a message that is known to the
	 *  bot and was edited. */
	Message          *edited_message;
	/*! Optional.  New incoming channel post of any kind. */
	Message          *channel_post;
	/*! Optional.  New version of a channel post that is known to
	 *  the bot and was edited. */
	Message          *edited_channel_post;
	/*! Optional.  New incoming inline query. */
	InlineQuery      *inline_query;
	/*! Optional.  The result of an inline query that was chosen
	 *  by a user and sent to their chat partner. */
	ChosenInlineResult *chosen_inline_result;
	/*! Optional.  New incoming callback query. */
	CallbackQuery    *callback_query;
	/*! Optional.  New incoming shipping query. */
	ShippingQuery    *shipping_query;
	/*! Optional.  New incoming pre-checkout query. */
	PreCheckoutQuery *pre_checkout_query;
	/*! Optional.  New poll state. */
	Poll             *poll;
};

/*! This object represents a Telegram user or bot. */
struct User_ {
	/*! Unique identifier for this user or bot. */
	int   id;
	/*! True, if this user is a bot. */
	bool  is_bot;
	/*! User's or bot's first name. */
	UChar *first_name;
	/*! Optional.  User's or bot's last name. */
	UChar *last_name;
	/*! Optional.  User's or bot's username. */
	UChar *username;
	/*! Optional.  IETF language tag of the user's language. */
	UChar *language_code;
};

/*! This object represents a venue. */
struct Venue_ {
	/*! Venue location. */
	Location *location;
	/*! Name of the venue. */
	UChar     *title;
	/*! Address of the venue. */
	UChar     *address;
	/*! Optional.  Foursquare identifier of the venue. */
	UChar     *foursquare_id;
	/*! Optional.  Foursquare type of the venue. */
	UChar     *foursquare_type;
};

/*! This object represents a video file. */
struct Video_ {
	/*! Unique identifier for this file. */
	UChar *file_id;
	/*! Video width as defined by sender. */
	int   width;
	/*! Video height as defined by sender. */
	int   height;
	/*! Duration of the video in seconds as defined by sender. */
	int   duration;
	/*! Optional.  Video thumbnail. */
	PhotoSize *thumb;
	/*! Optional.  Mime type of a file as defined by sender. */
	UChar *mime_type;
	/*! Optional.  File size. */
	int   file_size;
};

/*! This object represents a video message. */
struct VideoNote_ {
	/*! Unique identifier for this file. */
	UChar *file_id;
	/*! Video width and height (diameter of the video message) as
	 *  defined by sender. */
	int   length;
	/*! Duration of the video in seconds as defined by sender. */
	int   duration;
	/*! Optional.  Video thumbnail. */
	PhotoSize *thumb;
	/*! Optional.  File size. */
	int   file_size;
};

/*! This object represents a voice note. */
struct Voice_ {
	/*! Unique identifier for this file. */
	UChar *file_id;
	/*! Duration of the audio in seconds as defined by sender. */
	int   duration;
	/*! Optional.  Mime type of the file as defined by sender. */
	UChar *mime_type;
	/*! Optional.  File size. */
	int   file_size;
};

void Animation_free(Animation *anim);
Animation *Animation_parse(json_object *json);

void Audio_free(Audio *audio);
Audio *Audio_parse(json_object *json);

void CallbackQuery_free(CallbackQuery *cbq);
CallbackQuery *CallbackQuery_parse(json_object *json);

void Chat_free(Chat *chat);
Chat *Chat_parse(json_object *json);

void ChatPhoto_free(ChatPhoto *cp);
ChatPhoto *ChatPhoto_parse(json_object *json);

void ChosenInlineResult_free(ChosenInlineResult *cir);
ChosenInlineResult *ChosenInlineResult_parse(json_object *json);

void Contact_free(Contact *c);
Contact *Contact_parse(json_object *json);

void Document_free(Document *doc);
Document *Document_parse(json_object *json);

void EncryptedCredentials_free(EncryptedCredentials *ec);
EncryptedCredentials *EncryptedCredentials_parse(json_object *json);

void EncryptedPassportElement_free(EncryptedPassportElement *epe);
EncryptedPassportElement *EncryptedPassportElement_parse(json_object *json);
void EncryptedPassportElementArray_free(EncryptedPassportElement **epea);
EncryptedPassportElement
	**EncryptedPassportElementArray_parse(json_object *json);

void Game_free(Game *game);
Game *Game_parse(json_object *json);

void InlineQuery_free(InlineQuery *iq);
InlineQuery *InlineQuery_parse(json_object *json);

void Invoice_free(Invoice *in);
Invoice *Invoice_parse(json_object *json);

void Location_free(Location *loc);
Location *Location_parse(json_object *json);

void MaskPosition_free(MaskPosition *mp);
MaskPosition *MaskPosition_parse(json_object *json);

void Message_free(Message *msg);
Message *Message_parse(json_object *json);

void MessageEntity_free(MessageEntity *me);
MessageEntity *MessageEntity_parse(json_object *json);
void MessageEntityArray_free(MessageEntity **mea);
MessageEntity **MessageEntityArray_parse(json_object *json);

void OrderInfo_free(OrderInfo *oi);
OrderInfo *OrderInfo_parse(json_object *json);

void PassportData_free(PassportData *pd);
PassportData *PassportData_parse(json_object *json);

void PassportFile_free(PassportFile *pf);
PassportFile *PassportFile_parse(json_object *json);
void PassportFileArray_free(PassportFile **pfa);
PassportFile **PassportFileArray_parse(json_object *json);

void PhotoSize_free(PhotoSize *ps);
PhotoSize *PhotoSize_parse(json_object *json);
void PhotoSizeArray_free(PhotoSize **psa);
PhotoSize **PhotoSizeArray_parse(json_object *json);

void Poll_free(Poll *p);
Poll *Poll_parse(json_object *json);

void PollOption_free(PollOption *po);
PollOption *PollOption_parse(json_object *json);
void PollOptionArray_free(PollOption **poa);
PollOption **PollOptionArray_parse(json_object *json);

void PreCheckoutQuery_free(PreCheckoutQuery *pcq);
PreCheckoutQuery *PreCheckoutQuery_parse(json_object *json);

void ShippingAddress_free(ShippingAddress *sa);
ShippingAddress *ShippingAddress_parse(json_object *json);

void ShippingQuery_free(ShippingQuery *sq);
ShippingQuery *ShippingQuery_parse(json_object *json);

void Sticker_free(Sticker *st);
Sticker *Sticker_parse(json_object *json);

void SuccessfulPayment_free(SuccessfulPayment *sp);
SuccessfulPayment *SuccessfulPayment_parse(json_object *json);

void Update_free(Update *upd);
Update *Update_parse(json_object *json);
void UpdateArray_free(Update **upda);
Update **UpdateArray_parse(json_object *json);

void User_free(User *usr);
User *User_parse(json_object *json);
void UserArray_free(User **usra);
User **UserArray_parse(json_object *json);

void Venue_free(Venue *ven);
Venue *Venue_parse(json_object *json);

void Video_free(Video *vid);
Video *Video_parse(json_object *json);

void VideoNote_free(VideoNote *vn);
VideoNote *VideoNote_parse(json_object *json);

void Voice_free(Voice *v);
Voice *Voice_parse(json_object *json);
