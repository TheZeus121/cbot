/* parse.h -- macros and functions used in Telegram API object parsing
 *            and destruction.
 * Copyright (C) 2019 Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Pubic License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/agpl.html>.
 */

#include <json_object.h>
#include <unicode/utypes.h>

#define STR(x)  _STR(x)
#define _STR(x) #x

#define CONCAT(x, y)  _CONCAT(x, y)
#define _CONCAT(x, y) x##y

#define json_type_int64 json_type_int

#define int_parse(j)     json_object_get_int(j)
#define int64_parse(j)   json_object_get_int64(j)
#define boolean_parse(j) json_object_get_boolean(j)
#define double_parse(j)  json_object_get_double(j)

UChar  *string_parse(json_object *json);

#define json_type_Animation            json_type_object
#define json_type_Audio                json_type_object
#define json_type_CallbackQuery        json_type_object
#define json_type_Chat                 json_type_object
#define json_type_ChatPhoto            json_type_object
#define json_type_ChosenInlineResult   json_type_object
#define json_type_Contact              json_type_object
#define json_type_Document             json_type_object
#define json_type_EncryptedCredentials json_type_object
#define json_type_EncryptedPassportElementArray json_type_array
#define json_type_Game               json_type_object
#define json_type_InlineQuery        json_type_object
#define json_type_Invoice            json_type_object
#define json_type_Location           json_type_object
#define json_type_MaskPosition       json_type_object
#define json_type_Message            json_type_object
#define json_type_MessageEntityArray json_type_array
#define json_type_OrderInfo          json_type_object
#define json_type_PassportData       json_type_object
#define json_type_PassportFile       json_type_object
#define json_type_PassportFileArray  json_type_array
#define json_type_PhotoSize          json_type_object
#define json_type_PhotoSizeArray     json_type_array
#define json_type_Poll               json_type_object
#define json_type_PollOptionArray    json_type_array
#define json_type_PreCheckoutQuery   json_type_object
#define json_type_ShippingAddress    json_type_object
#define json_type_ShippingQuery      json_type_object
#define json_type_Sticker            json_type_object
#define json_type_SuccessfulPayment  json_type_object
#define json_type_User               json_type_object
#define json_type_UserArray          json_type_array
#define json_type_Venue              json_type_object
#define json_type_Video              json_type_object
#define json_type_VideoNote          json_type_object
#define json_type_Voice              json_type_object

#define MANDATORY(key, type) \
	if (!json_object_object_get_ex(json, STR(key), &jobj)) { \
		eprintf("User JSON doesn't contain '%s' field\n", STR(key)); \
		eprintf("    '%s'\n", json_object_to_json_string(json)); \
		goto err; \
	} \
	\
	if (!json_object_is_type(jobj, CONCAT(json_type_, type))) { \
		eprintf("'%s' is not %s!\n", STR(key), STR(type)); \
		eprintf("    '%s'\n", json_object_to_json_string(json)); \
		goto err; \
	} \
	\
	ret->key = CONCAT(type, _parse)(jobj);

#define OPTIONAL3(key, type, def) \
	if (json_object_object_get_ex(json, STR(key), &jobj)) { \
		if (!json_object_is_type(jobj, CONCAT(json_type_, type))) { \
			eprintf("'%s' is not %s!\n", STR(key), STR(type)); \
			eprintf("    '%s'\n", \
			        json_object_to_json_string(json)); \
			goto err; \
		} \
		\
		ret->key = CONCAT(type, _parse)(jobj); \
	} else { \
		ret->key = def; \
	}

#define OPTIONAL(key, type) OPTIONAL3(key, type, NULL)

#define FREE(var, func) if (var != NULL) { func(var); }
