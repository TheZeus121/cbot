# Makefile for cbot
# Copyright (C) 2019 Uko Koknevics
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/agpl.html>.

include config.mk

NAME := cbot

CFLAGS += -I h -I build/h

SRCS := $(shell find c -type f -iname '*.c')
DEPS := $(patsubst c/%.c,build/dep/%.mk,$(SRCS))
OBJS := $(patsubst c/%.c,build/obj/%.o,$(SRCS))

all: $(NAME)

clean:
	@echo "    CLEANING"
	@rm -rf build $(NAME)

love:
	@echo "Not war"

.PHONY: all clean love


$(SRCS): build/h/config.h

build/dep/%.mk: c/%.c
	@echo "    GENDEP $*"
	@mkdir -p $(@D)
	@$(CC) $(CFLAGS) -MF $@ $< -MM -MT "$@ build/obj/$*.o" -MP

build/obj/%.o: c/%.c
	@echo "    CC $*"
	@mkdir -p $(@D)
	@$(CC) $(CFLAGS) -c -o $@ $<

$(NAME): $(OBJS)
	@echo "    CCLD $@"
	@$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

build/h/%.h: h/%.h.in
	@echo "    GEN $*.h"
	@mkdir -p $(@D)
	@sed -e 's/@VERSION@/$(VERSION)/g' < $< > $@

-include $(DEPS)
