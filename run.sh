#!/bin/sh

set -e

echo "Killing the bot"
./kill.sh
echo "Compiling the bot..."
make -j "$(nproc)"
echo "Starting the bot..."
nohup ./cbot >out.log 2>err.log &
