VERSION := 0.1.0

CC ?= gcc

DEPENDENCIES := json-c libcurl icu-uc

CFLAGS  += -std=c99 -Wall -Wextra -pedantic -D_XOPEN_SOURCE=700 \
           $(shell pkg-config --cflags $(DEPENDENCIES))
LDFLAGS += -s $(shell pkg-config --libs $(DEPENDENCIES))
